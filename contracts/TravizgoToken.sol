// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./interface/IERC20.sol";
import "hardhat/console.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract TravizgoToken is IERC20, Ownable {
     struct PAY {
        uint expired;
        uint amount;
    }
    mapping(address => uint256) private _balances;

    mapping(address => mapping(address => uint256)) private _allowances;
    // account => expired => amount
    mapping (address => mapping(uint => uint)) public points;
    // account  expired[]
    mapping (address=> uint[]) public users;
   
    uint256 private _totalSupply;

    string private _name = "Travizgo Token";
    
    string private _symbol = "Tvg";

    modifier update(address account) {
        _;
        uint[] storage user = users[account];
        for (uint256 index = 0; index < user.length;) {
            uint expiresOn = user[index];
            if(expiresOn <= block.timestamp || points[account][expiresOn] == uint(0)){
                user[index] = user[user.length-1];
                user.pop();
            }else{
                index=index+1;
            }
        }
    }

    constructor() {}

    function name() public view virtual override returns (string memory) {
        return _name;
    }

    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    function decimals() public view virtual override returns (uint8) {
        return 18;
    }

    function totalSupply() public view virtual override returns (uint256) {
        return _totalSupply;
    }

    function transfer(address to, uint256 amount) public virtual override returns (bool) {
        address owner = _msgSender();
        _transfer(owner, to, amount);
        return true;
    }
    function pointsLength(address account) public view returns (uint length){
       length = users[account].length;
    }

    function userPoints(address account) public view returns(PAY[] memory returnData,uint timestamp) {
        uint[] storage user = users[account];
        returnData = new PAY[](user.length);
        for(uint256 i = 0; i < user.length; i++) {
            returnData[i] = PAY(user[i],points[account][user[i]]);
        }
        timestamp = block.timestamp;
    }

    function userPoints(address account,uint start, uint end) public view returns(PAY[] memory returnData,uint timestamp) {
        uint[] storage user = users[account];
        returnData = new PAY[](user.length);
        for(uint256 i = start; i < end; i++) {
            returnData[i] = PAY(user[i],points[account][user[i]]);
        }
        timestamp = block.timestamp;
    }

    function mintPoint(address account, uint amount, uint expires) public update(account) {
        points[account][expires] = amount;
        users[account].push(expires);
        emit mintTokenExpires(account, amount, expires);
        emit Transfer(address(0), account, amount);

    }

    function burnPoint(PAY[] memory pointPAY,address account) public update(account) {
        uint totalpay;
        for(uint256 i = 0; i < pointPAY.length; i++) {
            PAY memory pay = pointPAY[i];
            require(pay.expired > block.timestamp,"point expired");
            require(pay.amount <= points[account][pay.expired],"Insufficient balance");
            points[account][pay.expired] = points[account][pay.expired] - pay.amount;
            totalpay += pay.amount;
        }
        emit BurnPoint(account, totalpay);
        emit Transfer(account, address(0), totalpay);
    }

    function pointBalanceOf(address account) public view returns( uint256) {
        uint[] memory user = users[account];
        uint total;
        for (uint256 i = 0; i < user.length; ) {
           uint expiresOn = user[i];
           if(expiresOn > block.timestamp){
                total += points[account][expiresOn];  
           }
           unchecked {
            i++;
           }
        }
        return total;
    }

    function balanceOf(address account) public view virtual override returns (uint256) {
        return _balances[account];
    }

    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        address owner = _msgSender();
        _approve(owner, spender, amount);
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        address spender = _msgSender();
        _spendAllowance(from, spender, amount);
        _transfer(from, to, amount);
        return true;
    }

    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool) {
        address owner = _msgSender();
        _approve(owner, spender, allowance(owner, spender) + addedValue);
        return true;
    }

    function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool) {
        address owner = _msgSender();
        uint256 currentAllowance = allowance(owner, spender);
        require(currentAllowance >= subtractedValue, "ERC20: decreased allowance below zero");
        unchecked {
            _approve(owner, spender, currentAllowance - subtractedValue);
        }
        return true;
    }
   
    function _transfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {
        require(from != address(0), "ERC20: transfer from the zero address");
        require(to != address(0), "ERC20: transfer to the zero address");

        _beforeTokenTransfer(from, to, amount);

        uint256 fromBalance = _balances[from];
        require(fromBalance >= amount, "ERC20: transfer amount exceeds balance");
        unchecked {
            _balances[from] = fromBalance - amount;
        }
        _balances[to] += amount;

        emit Transfer(from, to, amount);

        _afterTokenTransfer(from, to, amount);
    }

    function _mint(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: mint to the zero address");

        _beforeTokenTransfer(address(0), account, amount);

        _totalSupply += amount;
        _balances[account] += amount;
        emit Transfer(address(0), account, amount);

        _afterTokenTransfer(address(0), account, amount);
    }

    function _burn(address account, uint256 amount) internal virtual {
        require(account != address(0), "ERC20: burn from the zero address");

        _beforeTokenTransfer(account, address(0), amount);

        uint256 accountBalance = _balances[account];
        require(accountBalance >= amount, "ERC20: burn amount exceeds balance");
        unchecked {
            _balances[account] = accountBalance - amount;
        }
        _totalSupply -= amount;

        emit Transfer(account, address(0), amount);

        _afterTokenTransfer(account, address(0), amount);
    }

    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function _spendAllowance(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        uint256 currentAllowance = allowance(owner, spender);
        if (currentAllowance != type(uint256).max) {
            require(currentAllowance >= amount, "ERC20: insufficient allowance");
            unchecked {
                _approve(owner, spender, currentAllowance - amount);
            }
        }
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

    function _afterTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual {}

}
