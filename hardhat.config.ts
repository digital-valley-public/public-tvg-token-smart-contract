import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-ethers";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";
import "hardhat-laika"; 
import "@ericxstone/hardhat-blockscout-verify";
import { SOLIDITY_VERSION, EVM_VERSION} from "@ericxstone/hardhat-blockscout-verify";
import "@nomiclabs/hardhat-web3";
dotenv.config();

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();
  for (const account of accounts) {
    console.log(account.address);
  }
});


const config: HardhatUserConfig = {
  defaultNetwork: "testnet_gbex",
  networks: {
    hardhat: {
      accounts: {
        count:5,
        mnemonic:
          process.env.MNEMONIC as string,
          accountsBalance:"100000000000000000000000000"
      },
      forking: {
        enabled: true,
        url: `https://bsc-dataseed.binance.org/`,
      },
    },
    localhost: {
      url: "http://127.0.0.1:8545",
      accounts: {
        mnemonic:
          process.env.MNEMONIC as string,
      },
    },
    testnet_gbex: {
      url: "https://testnet.gbex.network/",
      chainId:11,
      accounts: {
        mnemonic:
          process.env.MNEMONIC as string,
      }
    },
    testnet_bnb: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545/",
      chainId:97,
      accounts: {
        mnemonic:
          process.env.MNEMONIC as string,
      }
    }
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts",
  },
  mocha: {
    timeout: 400000000000000000000000,
  },
  solidity: {
    compilers: [  { version: "0.8.2" }, { version: "0.8.0" }, { version: "0.8.1" }, ] ,
    settings: {
      optimizer: {
        enabled: true,
        runs: 800,
      },
      metadata: {
        bytecodeHash: "none",
      },
    },
  },
  blockscoutVerify: {
    blockscoutURL: "https://testnet.gbexscan.com/api",
    contracts:{
    }
  },
};

export default config;
