import { TravizgoToken__factory } from './../typechain/factories/TravizgoToken__factory';
import { Contract } from 'ethers';
import hre, { artifacts } from 'hardhat';
import { Wallet } from 'ethers';
import { ethers } from "hardhat";
import { formatEther } from 'ethers/lib/utils';
let team: Wallet, 
investors: Wallet,
lockGbex:any;
import { runTypeChain, glob } from 'typechain'
import { saveFrontendFiles, typeChainToFrontend } from './utils/fileToFrontend';
import { TravizgoToken } from '../typechain';

async function main() {
    ;[  
      team,
      investors
      ] = await (ethers as any).getSigners()
  const ERC20TravizgoToken = await ethers.getContractFactory('TravizgoToken',team) as TravizgoToken__factory;
  let travizgoToken: TravizgoToken = await ERC20TravizgoToken.deploy()
  await travizgoToken.deployed()
  saveFrontendFiles([
    {
      key:"TravizgoToken",
      contract:travizgoToken
    }
  ])
  // await typeChainToFrontend()
  // await hre.run("laika-sync", {
  //   contract: "LockGBEX",
  //   address: lockGbex.address,
  // })
}



main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
});


