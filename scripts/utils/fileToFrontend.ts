import { glob, runTypeChain } from 'typechain';
import { Contract } from 'ethers';
import path from 'path';
const fs = require("fs");

type  paramArrToFrontend = {
    key: string,
    contract:Contract
}

export async function saveFrontendFiles(arContract:paramArrToFrontend[]) {
  const Dir = path.join(path.resolve("./"),process.env.ROOT_PATHFRONTEND as string,process.env.DIR_FRONTEND_ADDRESS as string) ;
  const pathFile = Dir + "/address.json"; 
  if (!fs.existsSync(Dir)) fs.mkdirSync(Dir,{ recursive: true });
  var obj:any = {}
  try {
    obj = (fs.existsSync(pathFile))?JSON.parse(fs.readFileSync(pathFile, 'utf8')):{};
  } catch (error) {}
  arContract.forEach(({ key,contract }) =>  obj[key] =  contract.address);
  fs.writeFileSync(
    pathFile,
    JSON.stringify(obj, undefined, 2)
  );
  console.table(arContract.map(({key,contract})=> { return { contractname: key, address:contract.address}}))
}

export async function typeChainToFrontend(){
  const cwd = path.join(path.resolve("./"),'artifacts');
  const allFiles = glob(cwd, [process.env.REGEX_PATH_TYPECHAIN as string])
  const Dir = path.join(path.resolve("./"),process.env.ROOT_PATHFRONTEND as string,process.env.DIR_FRONTEND_TYPECHAIN as string) 
  await runTypeChain({
    cwd,
    filesToProcess: allFiles,
    allFiles,
    outDir:  Dir,
    target: 'ethers-v5',
  })
}