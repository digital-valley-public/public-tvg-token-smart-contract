import { formatEther, parseEther } from 'ethers/lib/utils';
import { expect } from "chai";
import { BigNumber, BigNumberish, Wallet } from "ethers";
import { ethers } from "hardhat";
import { TravizgoToken, TravizgoToken__factory } from '../typechain';
const { BN, expectEvent, expectRevert, time } = require('@openzeppelin/test-helpers');
let team:Wallet, investors:Wallet;
let travizgoToken:TravizgoToken;
let blockNumber:any;
let expires:number;
describe("TravizgoToken", function () {
  before("create fixture loader", async () => {
    ;[  
          team,
          investors
          ] = await (ethers as any).getSigners()
      const ERC20TravizgoToken = await ethers.getContractFactory('TravizgoToken',team) as TravizgoToken__factory;
      travizgoToken = await ERC20TravizgoToken.deploy()
      await travizgoToken.deployed()
      console.log(travizgoToken.address)
      blockNumber = await time.latest();
      expires = Date.now() / 1000+ 100 | 0

  });

  it("mint 6000 tvg", async () => {
    const tx  =  await travizgoToken.mintPoint(team.address,parseEther('1000'), expires)
    const res  = await tx.wait()
    await (await travizgoToken.mintPoint(team.address,parseEther('3000'), expires+400)).wait()
    await (await travizgoToken.mintPoint(team.address,parseEther('2000'), expires+100)).wait()
    console.log(formatEther(await travizgoToken.pointBalanceOf(team.address)))
    expect(formatEther(await travizgoToken.pointBalanceOf(team.address))).equal('6000.0')
  })

  it("pay point",async () => {
    const userPoints = await travizgoToken['userPoints(address)'](team.address)
    const paydata = filtersPayPoint(1500,userPoints)
    const tx = await travizgoToken.burnPoint(paydata,team.address)
    const res = await tx.wait()
    console.log(formatEther(await travizgoToken.pointBalanceOf(team.address)))
    expect(formatEther(await travizgoToken.pointBalanceOf(team.address))).equal('4500.0')
  })
  
  it("increaseto to expires", async () => {
    await time.increaseTo(new BN(String(expires)));
    console.log(formatEther(await travizgoToken.pointBalanceOf(team.address)))
    expect(formatEther(await travizgoToken.pointBalanceOf(team.address))).equal('4500.0')
  })

  it("increaseto to expires + 100", async () => {
    await time.increaseTo(new BN(String(expires+100)));
    console.log(formatEther(await travizgoToken.pointBalanceOf(team.address)))
    expect(formatEther(await travizgoToken.pointBalanceOf(team.address))).equal('3000.0')

  })

  it("increaseto to expires + 400", async () => {
    const userPoints = await travizgoToken['userPoints(address)'](team.address)
    const paydata = filtersPayPoint(1500,userPoints)
    const tx = await travizgoToken.burnPoint(paydata,team.address)
    const res = await tx.wait()
    // console.log((await travizgoToken['userPoints(address)'](team.address)).returnData)
    await time.increaseTo(new BN(String(expires+400)));
    console.log(formatEther(await travizgoToken.pointBalanceOf(team.address)))
  })

});



type PointData = [
  ([BigNumber, BigNumber] & { expired: BigNumber; amount: BigNumber })[],
  BigNumber
] & {
  returnData: ([BigNumber, BigNumber] & {
    expired: BigNumber;
    amount: BigNumber;
  })[];
  timestamp: BigNumber;
}
function filtersPayPoint(amount:number, userPoints:PointData):{
  expired: BigNumberish;
  amount: BigNumberish;
}[]{
  let amount18 = parseEther(`${amount}`);
  return userPoints.returnData
  .map((a) => ({expired:Number(a.expired),amount:a.amount}))
  .sort((a,b)=> a.expired - b.expired)
  .reduce<{expired:BigNumberish,amount:BigNumberish}[]>((previousValue, currentValue)=>{
     if(amount18.eq('0')||currentValue.amount.eq('0')||userPoints.timestamp.gte(currentValue.expired)) return previousValue;
     else if(currentValue.amount.gte(amount18)){
       previousValue.push({...currentValue,amount:`${amount18}`})
       amount18 = amount18.sub(amount18);
       return previousValue
     }
     amount18 = amount18.sub(currentValue.amount);
     previousValue.push({...currentValue,amount:`${currentValue.amount}`})
     return previousValue
  },[])
}