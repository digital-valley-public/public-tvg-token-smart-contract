import { formatEther, parseEther } from 'ethers/lib/utils';
import { mintPoint, account, burnPoint, pointBalanceOf, TVG } from './Tvg';


(async ()=>{

    //ex. mint point
    let  expires = Date.now() / 1000 + 5000 | 0;
    const { status:statusMint, ...resMint } = await mintPoint(account.address , String(parseEther('2000')), String(expires))

    //ex. burn point
    const { status:statusBurn, ...resBurn } = await burnPoint(account.address,parseEther('1000'))

    //ex. get balance point
    console.log(formatEther(await pointBalanceOf(account.address)))

})()

// const returnData = await TVG['userPoints(address)'](account.address)
// console.log(returnData)