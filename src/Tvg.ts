import { BigNumberish } from 'ethers';
import { BigNumber, ContractReceipt } from 'ethers';
import { TravizgoToken__factory } from '../typechain/factories/TravizgoToken__factory';
import { formatUnits, parseEther } from 'ethers/lib/utils';
import { ethers } from 'ethers';

type address = string;
type PointData = [
    ([BigNumber, BigNumber] & { expired: BigNumber; amount: BigNumber })[],
    BigNumber
  ] & {
    returnData: ([BigNumber, BigNumber] & {
      expired: BigNumber;
      amount: BigNumber;
    })[];
    timestamp: BigNumber;
}

var Provider = new ethers.providers.JsonRpcProvider("https://testnet.gbex.network/");
const Seed  = 'nose couch initial asthma smooth raccoon say tape tennis enemy era trumpet'  // WALLET SEEDPHRASE
const wallet    = ethers.Wallet.fromMnemonic(Seed);

export const account   = wallet.connect(Provider);
export const TVG =  TravizgoToken__factory.connect('0x96DC5EEe5Ca287eDE9BE66A9f0475CB043ca248B', account);



export async function pointBalanceOf(account:address): Promise<BigNumber>{
    return await TVG.pointBalanceOf(account)
}

export async function mintPoint(account:address, amount:BigNumberish, expires: BigNumberish):Promise<ContractReceipt>{
    return new Promise(async function(Resolve, Reject) {
        try {
            const gasLimit =  await TVG.estimateGas.mintPoint(account,amount,expires,{})
            const tx =  await TVG.mintPoint(account,amount,expires,{ gasLimit })
            return Resolve(await tx.wait())
        } catch (error) {
            return Reject({error})
        }
    })
}

export async function burnPoint(account:address,amount:BigNumber):Promise<ContractReceipt>{
    return new Promise(async function(Resolve, Reject) {
        try {
            const returnData = await TVG['userPoints(address)'](account)
            const {data:pointPAY,status} =  filtersPayPoint(amount,returnData)
            if(!status || !pointPAY.length){
                return Reject(new Error('Insufficient point balance'))
            }
            const gasLimit =  await TVG.estimateGas.burnPoint(pointPAY,account,{})
            const tx =  await TVG.burnPoint(pointPAY,account,{ gasLimit })
            return Resolve(await tx.wait())
        } catch (error) {
            return Reject({error})
        }
    })
}

function filtersPayPoint(amount:BigNumber, userPoints:PointData):{
    data:{
        expired: BigNumberish;
        amount: BigNumberish;
    }[]
    status:boolean
    }{
    return {
        data: userPoints.returnData
            .map((a) => ({expired:Number(a.expired),amount:a.amount}))
            .sort((a,b)=> a.expired - b.expired)
            .reduce<{expired:BigNumberish,amount:BigNumberish}[]>((previousValue, currentValue) => {
                if(amount.eq('0')||currentValue.amount.eq('0')||userPoints.timestamp.gte(currentValue.expired)) return previousValue;
                else if(currentValue.amount.gte(amount)){
                    previousValue.push({...currentValue,amount:`${amount}`})
                    amount = amount.sub(amount);
                    return previousValue
                }
                amount = amount.sub(currentValue.amount);
                previousValue.push({...currentValue,amount:`${currentValue.amount}`})
                return previousValue
            },[]),
        status: amount.eq('0')
    }
}